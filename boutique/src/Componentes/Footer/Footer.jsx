import React from "react";
import { RttRounded } from "@mui/icons-material";
import { Grid, Link, Typography } from "@mui/material";
import FacebookIcon from '@mui/icons-material/Facebook';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import YouTubeIcon from '@mui/icons-material/YouTube';

const Footer = () => {
    return(
        <>

    <footer className="footer">
        <Grid container spacing={2}>
            <Grid item container justifyContent={"space-around"}>
                <Grid>
                     <h2>
                        Sobre Nosotros
                    </h2>
                     <Typography>¿Quiénes somos?</Typography>
                     <Typography>Conoce nuestros horarios</Typography>
                     <Typography>Blogs</Typography>
                </Grid>
            <Grid>
                    < h2>
                        Visitános en nuestras redes sociales
                    </h2>
                    <FacebookIcon style={{padding:"10px", color: "white"}}/>
                    <WhatsAppIcon style={{padding:"10px", color: "white"}}/>
                    <YouTubeIcon style={{padding:"10px", color: "white"}}/>
            </Grid>
            <Grid>
                    < h2>
                        Encuéntranos en
                    </h2>
                    <FacebookIcon style={{padding:"10px", color: "white"}}/>
            </Grid>
            
                </Grid>
             <Grid item container justifyContent={"center"}>
                 <Grid item sm={12}>
                    <Typography variant="body">Copyright© 2022 Coco's Boutique | Todos los derechos reservados</Typography>
                 </Grid>
                 <Grid item  sm={12}>
                 <Link href="#" style={{color: "yellow"}}>vallecillolynoska@gmailcom | mendezpatris@outlook.com | evaldez202@yahoo.com</Link>
                 </Grid>
            </Grid>
            <br/>
        </Grid>
    </footer>
    </>
    );
}

export default Footer;