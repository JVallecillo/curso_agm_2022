import React, {useState} from "react";
import { Grid, Box, Toolbar, Tabs, Tab, IconButton, Typography, Menu, Container, MenuItem } from "@mui/material";
import TabContext from "@mui/lab/TabContext";
import TabPanel from "@mui/lab/TabPanel";
import MenuIcon from "@mui/icons-material/Menu";
import Home from "./Componentes/Presentacion/Home/Home";
import './App.css';
import Footer from "./Componentes/Footer/Footer";

const pages = ['Home'];

const App = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [numOpcion, setNumOpcion] = useState(0);

  const handleOpenNavMenu = (event) =>{
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleTableChange = (event, index) => {
    setNumOpcion(index);
  };
  return (
    <Grid container maxWidth="xl" sx={{border: 1, borderColor: 'divider', borderRadius: 3}}>
      <Grid item container xs={12} >
        <TabContext value={numOpcion}>
        <Toolbar disableGutters>
          <Typography
            variant="h5"
            noWrap
            component="div"
            sx={{ display: { xs: 'flex', md:'none'} }}
            >
              Coco's Boutique
            </Typography>
            <Box sx={{ flexGrow: 4, display: { xs: 'flex', md: 'none' }}}>
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup='true'
                  onClick={handleOpenNavMenu}
                  color="inherit"
                >
                  <MenuIcon />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorElNav}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal:'left',
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: 'top',
                    horizontal:'left',
                  }}
                  open={Boolean(anchorElNav)}
                  onClose={handleCloseNavMenu}
                  sx={{
                    display: { xs: 'block', md:'none'},
                  }}
                >
                  {pages.map((page) => (
                    <MenuItem key={page} onClick={handleCloseNavMenu}>
                      <Typography textAlign="center">{page}</Typography>
                    </MenuItem>
                  ))}
                </Menu>
                </Box>
                <Typography
                    variant="h5"
                    noWrap
                    component="div"
                    sx={{mr: 2, color:"primary", display:{xs: 'none', md: 'flex'}}}
                  >
                    Coco's Boutique
                  </Typography>
                  <Box sx={{ flexGrow: 1, display: {xs: 'none', md: 'flex'}, borderBottom: 1, borderColor: 'divider'}}>
                    <Tabs
                      indicatorColor="primary"
                      onChange={handleTableChange}
                      textColor="primary"
                      value={numOpcion}
                    >
                      <Tab
                        label="Home">  
                      </Tab>

                      <Tab
                        label="Categoria">  
                      </Tab>

                      <Tab
                        label="Nuestras Ofertas">  
                      </Tab>

                      <Tab
                        label="Contáctanos">  
                      </Tab>

                    </Tabs>
                  </Box>

          </Toolbar>
          <br/>
            <TabPanel value={numOpcion} index={0}>
              <Home/>
            </TabPanel>

           {/*  <TabPanel value={numOpcion} index={1}>
              <Categorias/>
            </TabPanel>

            <TabPanel value={numOpcion} index={2}>
              <Ofertas/>
            </TabPanel>

            <TabPanel value={numOpcion} index={3}>
              <Contáctanos/>
            </TabPanel> */}
        </TabContext>

      </Grid>
      <br/>
      <Grid container>
        <Footer/>
      </Grid>
  
  </Grid>
  );
};

export default App;
